sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Form.controller.RealForm", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nnext.iq.Form.view.RealForm
		 */
		onInit: function() {
			this.oSubmitData = null;
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("Flow7", "FORM_DATA_CHANGE", this.endpointProcess, this);
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf nnext.iq.Form.view.RealForm
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf nnext.iq.Form.view.RealForm
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf nnext.iq.Form.view.RealForm
		 */
		//	onExit: function() {
		//
		//	}
		endpointProcess: function(sChanel, sEvent, oData) {
			console.log("subscribe: ");
			console.log(oData);
			this.oSubmitData = oData;
		},
		onSubmit: function(){
			console.log("submit");
			console.log(this.oSubmitData);
		}
	});

});