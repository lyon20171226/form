sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Form.controller.RealFormEmbedded", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nnext.iq.Form.view.RealFormEmbedded
		 */
		//	onInit: function() {
		//
		//	},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf nnext.iq.Form.view.RealFormEmbedded
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf nnext.iq.Form.view.RealFormEmbedded
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf nnext.iq.Form.view.RealFormEmbedded
		 */
		//	onExit: function() {
		//
		//	}
		onCitiesRequest: function() {
			if (!this._oCitiesDialog) {
				// this._oCitiesDialog = sap.ui.xmlfragment("nnext.iq.Form.view.xxx", this);
				this._oCitiesDialog = this.getView().byId("idCities");
			}

			this._oCitiesDialog.open();
		},
		onCityChange: function(oEvent){
			var sTitle = oEvent.getParameter("selectedItem").getProperty("title");
			
			var oModel = this.getView().getModel("user");

			oModel.setProperty("/address/city", sTitle);
			this.getView().setModel(oModel, "user");
			
			console.log(this.getOwnerComponent().getModel("user"));
			this.publishDataToHost(oModel);
		},
		publishDataToHost: function(oData){
			sap.ui.getCore().getEventBus().publish("Flow7", "FORM_DATA_CHANGE", oData);
		}
	});

});