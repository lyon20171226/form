sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Form.controller.DemoMenu", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nnext.iq.Form.view.DemoMenu
		 */
		onInit: function() {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf nnext.iq.Form.view.DemoMenu
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf nnext.iq.Form.view.DemoMenu
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf nnext.iq.Form.view.DemoMenu
		 */
		//	onExit: function() {
		//
		//	}
		showForm: function() {
			this.oRouter.navTo("Form");
		},
		showSimple: function() {
			this.oRouter.navTo("Simple");
		},
		showFragment: function(){
			this.oRouter.navTo("Fragment");
		},
		showEmbedded: function(){
			this.oRouter.navTo("Embedded");
		},
		showReal: function(){
			this.oRouter.navTo("RealForm");
		}
	});

});