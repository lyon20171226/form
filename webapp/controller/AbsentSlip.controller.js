sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/m/MessageToast'
], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("nnext.iq.Form.controller.AbsentSlip", {
		onInit: function() {
			var self = this;
			var a1 = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			var a2 = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			var a3 = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			var a4 = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			var a5 = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			var a6 = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			var a7 = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			var a8 = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
			var RequisitionID = a1 + a2 + "-" + a3 + "-" + a4 + "-" + a5 + "-" + a6 + a7 + a8;
			var oMasters = new sap.ui.model.json.JSONModel();
			var oTimes = new sap.ui.model.json.JSONModel();
			var oLeaves = new sap.ui.model.json.JSONModel();
			var oMembers = new sap.ui.model.json.JSONModel();
			var oDepartments = new sap.ui.model.json.JSONModel();

			oMasters.loadData("model/oMaster.json");
			oTimes.loadData("model/oTime.json");
			oLeaves.loadData("model/oLeave.json");
			oMembers.loadData("model/oMembers.json");
			oDepartments.loadData("model/oDepartments.json");

			oMasters.attachRequestCompleted(function() {
				oMasters.getProperty("/M").RequisitionID = RequisitionID;
				self.getView().setModel(oMasters, "oMasters");
			});

			oTimes.attachRequestCompleted(function() {
				self.getView().setModel(oTimes, "oTimes");
			});

			oLeaves.attachRequestCompleted(function() {
				self.getView().setModel(oLeaves, "oLeaves");
			});

			oMembers.attachRequestCompleted(function() {
				self.getView().setModel(oMembers, "oMembers");
				//console.log(oMembers.getProperty("/")[0]);
			});

			oDepartments.attachRequestCompleted(function() {
				self.getView().setModel(oDepartments, "oDepartments");
				//console.log(oDepartments.getProperty("/children"));
			});

			//this.getView().getModel("oMasters").getProperty("/M").RequisitionID = RequisitionID;
			//this.getView().byId("_theStartTime").setSelectedKey("9");
			//this.getView().byId("_theEndTime").setSelectedKey("9");
			//this.getView().byId("ApplicantDept").setSelectedKey("dede3adf-0a35-4613-a150-db1036053bc4");
		},
		onDeptChange: function() {
			var self = this;
			var sDeptID = this.getView().byId("ApplicantDept").getSelectedKey();
			var oMembers = new sap.ui.model.json.JSONModel();
			//var oMembers = this.getView().getModel("oMembers");

			oMembers.loadData("model/oMembers.json");

			oMembers.attachRequestCompleted(function() {
				var oMembersProperty = oMembers.getProperty("/");
				var aTmp = [];
				for (var i = 0; i < oMembersProperty.length; i++) {
					console.log(sDeptID);
					if (oMembersProperty[i].departmentId === sDeptID) {
						aTmp.push(oMembersProperty[i]);
					}
				}
				self.getView().getModel("oMembers").setProperty("/", aTmp);
			});

		},
		onDateChange: function() {
			var oStartDate = this.getView().byId("_theStartDate").getDateValue();
			//var sStartTime = this.getView().byId("_theStartTime").getValue();
			var oEndDate = this.getView().byId("_theEndDate").getDateValue();
			var oTotalHour = this.getView().byId("_theTotalHour");
			//var sEndTime = this.getView().byId("_theEndTime").getValue();

			this.getView().byId("_theEndDate").setMinDate(new Date(oStartDate));

			if (oStartDate !== null && oEndDate !== null) {
				var iDiffDate = Math.abs(oStartDate.getTime() - oEndDate.getTime());
				var iDiffDateD = Math.ceil(iDiffDate / (1000 * 60 * 60 * 24));
				console.log((iDiffDateD + 1) * 8);
				oTotalHour.setValue((iDiffDateD + 1) * 8);
				//console.log(iDiffTimeD);

			}

		},
		onSubmit: function() {
			MessageToast.show(JSON.stringify(this.getView().getModel("oMasters").getProperty("/M")));
		}
	});
});